package ClientCalculate;

import java.util.Scanner;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA_2_3.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import CalculationApp.Calculation;
import CalculationApp.CalculationHelper;

public class ClientSideCalculate {
	
	static Calculation calImpl;
	
	public static void main(String args[])
	{
		
		try {
		    // create and initialize the ORB
			ORB orb = (ORB) ORB.init(args,null);	
	        
			//get the root naming context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			
			 // Use NamingContextExt instead of NamingContext. This is 
	        // part of the Interoperable naming Service.  
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			
			// resolve the Object Reference in Naming
	        String name = "Hello";
	        calImpl = CalculationHelper.narrow(ncRef.resolve_str(name));
	        
	        System.out.println("Obtained a handle on server object: " + calImpl);
	        
	        Scanner c=new Scanner(System.in);
	        
	        int num1 = 0,num2 = 0, flag = 1;
	        char opr;
	       
	       	while(flag == 1)
	       	{
	       		try{
	        		//Get First Number from user
	        		System.out.println("Enter the first number");
	        		String val1 = c.nextLine();
	        		num1 = Integer.parseInt(val1);
	            
	        		//Get Second Number from user
	        		System.out.println("Enter the second number");
	        		String val2 = c.nextLine();
	        		num2 = Integer.parseInt(val2);
	            
	        		//Get the operator that needs to be used
	        		System.out.println("Enter the operation to be performed");
	        		String opr_val = c.nextLine();		        
	        		opr = opr_val.charAt(0);
		        
	        		//get the result from the server method 'calculate'
	        		int result = calImpl.calculate(num1, num2, opr);
		        
	        		//Check the result
	        		if(result == -1)
	        		{
	        			System.out.println("Please enter a valid operator");
	        			continue;
	        		}
	        		else
	        		{
	        			System.out.println("The result is "+result);
	        		}
	        		System.out.println("-------------------------------------------------");
		        
	        		System.out.println("Do you wish to continue? YES or NO");
	        		String ans = c.nextLine();
		        
	        		if(ans.equals("YES"))
	        		{
	        			flag = 1;
	        		}
	        		else if(ans.equals("NO")){
	        			flag = 0;
	        		}
	        		else{
	        			System.out.println("Invalid Entry");
	        		}
	        	}
	       		 catch(NumberFormatException e)
	 	        {
	 	        	System.out.println("Please enter a valid number");
	 	        	continue;
	 	        }
  	
	        	if(flag == 0)
	        	{
	        		calImpl.shutdown();	        		
	        	}
	        }
	       	                
		} catch (InvalidName e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotProceed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
