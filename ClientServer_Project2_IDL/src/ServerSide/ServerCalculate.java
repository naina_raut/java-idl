package ServerSide;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import CalculationApp.Calculation;
import CalculationApp.CalculationHelper;
import CalculationApp.CalculationPOA;
	
	class ImplementCalculate extends CalculationPOA{
			
		private ORB orb;
    	
		//set the ORB object
		public void setOrb(ORB orb_val) {
			orb = orb_val;
		}

		//Calculate function to calculate the result based on input parameters
		@Override
		public int calculate(int a, int b, char opr) {
			
			switch (opr)
			{
				case '+':
					return a + b;
				case '-':
					return a - b;
				case '/':
					return a / b;
				case '*':
					return a * b;
				case '%':
					return a % b;
			    default:
			    	return -1;
			}			
		}
        
		//Shutdown method to shutdown the server and clean all the objects	
		@Override
		public void shutdown() {
			try{
				System.out.println("ORB is shutting down");
				orb.shutdown(false);
				orb.destroy();
			}
			catch(Exception e)
			{
				
			}
		}
		
	}
		
	
    public class ServerCalculate {
    	
    	public static void main(String args[])
    	{
    		try{
    			
    		  // create and initialize the ORB
    	      ORB orb = ORB.init(args, null);

    	      // get reference to rootpoa & activate the POAManager
    	      POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
    	      rootpoa.the_POAManager().activate();

    	      // create servant and register it with the ORB
    	      ImplementCalculate calculateImp = new ImplementCalculate();
    	      calculateImp.setOrb(orb); 

    	      // get object reference from the servant
    	      org.omg.CORBA.Object ref = rootpoa.servant_to_reference(calculateImp);
    	      Calculation href = CalculationHelper.narrow(ref);
    		  
    	      // get the root naming context
    	      org.omg.CORBA.Object objRef =
    	          orb.resolve_initial_references("NameService");
    	      
    	      // Use NamingContextExt which is part of the Interoperable
    	      // Naming Service (INS) specification.
    	      NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

    	      // bind the Object Reference in Naming
    	      String name = "Hello";
    	      NameComponent path[] = ncRef.to_name( name );
    	      ncRef.rebind(path, href);

    	      System.out.println("CalculateServer ready and waiting ...");

    	      // wait for invocations from clients
    	      orb.run();
    		}
    		catch (Exception e) {
    	        System.err.println("ERROR: " + e);
    	        e.printStackTrace(System.out);
    	      }
    		System.out.println("CalculateServer Exiting ...");
    	}
    	
    }

